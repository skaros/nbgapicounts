package api;
import java.net.URI;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.json.parsers.JSONParser;
import com.json.parsers.JsonParserFactory;

public class GetApis {
	public String id="a82c147ab5c846c18420b18ed2156510";
	

	public void setId(String id){
		this.id=id;
	}
	
	
	    public int countAccounts()
	    {
	        HttpClient httpclient = HttpClients.createDefault();

	        try
	        {
	            URIBuilder builder = new URIBuilder("https://nbgdemo.azure-api.net/testnodeapi/api/accounts/list");


	            URI uri = builder.build();
	            HttpGet request = new HttpGet(uri);
	            request.setHeader("Ocp-Apim-Subscription-Key", id);

	            HttpResponse response = httpclient.execute(request);
	            HttpEntity entity = response.getEntity();
	            
	            String json=EntityUtils.toString(entity);
	            JSONObject obj = new JSONObject(json);
	            JSONArray arr = obj.getJSONArray("accounts");
	           
	            return arr.length();
	        }
	        catch (Exception e)
	        {
	            System.out.println(e.getMessage());
	        }
	        return -1;
	    }

	    public int countAtms(){
	    	 HttpClient httpclient = HttpClients.createDefault();

	         try
	         {
	             URIBuilder builder = new URIBuilder("https://nbgdemo.azure-api.net/testnodeapi/api/atms/list");


	             URI uri = builder.build();
	             HttpGet request = new HttpGet(uri);
	             request.setHeader("Ocp-Apim-Subscription-Key",id);



	       

	            HttpResponse response = httpclient.execute(request);
	            HttpEntity entity = response.getEntity();
	           
	            String json=EntityUtils.toString(entity);
	            JSONObject obj = new JSONObject(json);
	            JSONArray arr = obj.getJSONArray("atms");
	           
	            return arr.length();
//	            if (entity != null) 
//	            {
//	                System.out.println(EntityUtils.toString(entity));
//	            }
	        }
	        catch (Exception e)
	        {
	            System.out.println(e.getMessage());
	      return -1;  }
			
	    }

	    public int countBranches(){
	    	HttpClient httpclient = HttpClients.createDefault();

	        try
	        {
	            URIBuilder builder = new URIBuilder("https://nbgdemo.azure-api.net/testnodeapi/api/branches/list");

	            URI uri = builder.build();
	            HttpGet request = new HttpGet(uri);
	            request.setHeader("Ocp-Apim-Subscription-Key", id);

	        
	            HttpResponse response = httpclient.execute(request);
	            HttpEntity entity = response.getEntity();
//	            branches
	            String json=EntityUtils.toString(entity);
	            JSONObject obj = new JSONObject(json);
	            JSONArray arr = obj.getJSONArray("branches");
	           
	            return arr.length();
	            
	           
	        }
	        catch (Exception e)
	        {
	            System.out.println(e.getMessage());
	            return -1;
	        }
	    }

	    public int countCustomers(){
	    	HttpClient httpclient = HttpClients.createDefault();

	        try
	        {
	            URIBuilder builder = new URIBuilder("https://nbgdemo.azure-api.net/testnodeapi/api/customers/list");


	            URI uri = builder.build();
	            HttpGet request = new HttpGet(uri);
	            request.setHeader("Ocp-Apim-Subscription-Key", id);


	       
	            HttpResponse response = httpclient.execute(request);
	            HttpEntity entity = response.getEntity();
	            String json=EntityUtils.toString(entity);
	            JSONObject obj = new JSONObject(json);
	            JSONArray arr = obj.getJSONArray("customers");
              
	            return arr.length();
	          
	        }
	        catch (Exception e)
	        {
	            System.out.println(e.getMessage());
	            return -1; 
	       }
			
	    }
	    public int countevents(){
	    	HttpClient httpclient = HttpClients.createDefault();

	        try
	        {
	            URIBuilder builder = new URIBuilder("https://nbgdemo.azure-api.net/testnodeapi/api/events/list");

	            URI uri = builder.build();
	            HttpGet request = new HttpGet(uri);
	            request.setHeader("Ocp-Apim-Subscription-Key", id);


	       
	            HttpResponse response = httpclient.execute(request);
	            HttpEntity entity = response.getEntity();
	            String json=EntityUtils.toString(entity);
	            JSONObject obj = new JSONObject(json);
	            JSONArray arr = obj.getJSONArray("events");
              System.out.println(json);
	            return arr.length();
	          
	        }
	        catch (Exception e)
	        {
	            System.out.println(e.getMessage());
	            return -1; 
	       }
			
	    }

	    public int countProducts(){
	    	HttpClient httpclient = HttpClients.createDefault();

	        try
	        {
	            URIBuilder builder = new URIBuilder("https://nbgdemo.azure-api.net/testnodeapi/api/products/list");

	            URI uri = builder.build();
	            HttpGet request = new HttpGet(uri);
	            request.setHeader("Ocp-Apim-Subscription-Key", id);


	       
	            HttpResponse response = httpclient.execute(request);
	            HttpEntity entity = response.getEntity();
	            String json=EntityUtils.toString(entity);
	            JSONObject obj = new JSONObject(json);
	            JSONArray arr = obj.getJSONArray("products");
              
	            return arr.length();
	          
	        }
	        catch (Exception e)
	        {
	            System.out.println(e.getMessage());
	            return -1; 
	       }
			
	    }

	    public int countTransactions(){
	    	HttpClient httpclient = HttpClients.createDefault();

	        try
	        {
	            URIBuilder builder = new URIBuilder("https://nbgdemo.azure-api.net/testnodeapi/api/transactions/list");
	            URI uri = builder.build();
	            HttpGet request = new HttpGet(uri);
	            request.setHeader("Ocp-Apim-Subscription-Key", id);


	       
	            HttpResponse response = httpclient.execute(request);
	            HttpEntity entity = response.getEntity();
	            String json=EntityUtils.toString(entity);
	            JSONObject obj = new JSONObject(json);
	            JSONArray arr = obj.getJSONArray("transactions");
              
	            return arr.length();
	          
	        }
	        catch (Exception e)
	        {
	            System.out.println(e.getMessage());
	            return -1; 
	       }
			
	    }

}
